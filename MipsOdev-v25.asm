#################################################################################################################################
	# Copyright (c) 2018, MUHAMMED SAID BILGEHAN
	# All rights reserved.

	# Redistribution and use in source and binary forms, with or without
	# modification, are permitted provided that the following conditions are met:
	# 1. Redistributions of source code must retain the above copyright
	#    notice, this list of conditions and the following disclaimer.
	# 2. Redistributions in binary form must reproduce the above copyright
	#    notice, this list of conditions and the following disclaimer in the
	#    documentation and/or other materials provided with the distribution.
	# 3. All advertising materials mentioning features or use of this software
	#    must take permission from MUHAMMED SAID BILGEHAN and must display the
	#	   following acknowledgement:
	#    This product includes software developed by the MUHAMMED SAID BILGEHAN.
	# 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
	#    names of its contributors may be used to endorse or promote products
	#    derived from this software without specific prior written permission.

	# THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
	# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	# DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
	# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
	# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#################################################################################################################################


.data
	isaret: .asciiz "\n\tIsaretler\n\n\t+ | - | * | /\n\t--------------\n"
	y1: .asciiz "\nYazi Giriniz: "
	y2: .asciiz "\n\tSonuc: "
	veri: .space 1024   # Kullanici Girdisi


	###########################################
	 # t0 yazi dizisinde karakterleri almak icin veri boslugunun baslangic adres sayaci
	 # t1 sayi kontrol karsilastirmada kullan
	 # t2 basamak karsilastirmada kullan
	 # t3 10 ile carpmak icin kullan
	 # t4 operatorler icin
	 # t5 Gecici sayi icin
	 # t6 1. sayi icin
	 # t7 2. sayi icin
	 # s0 sonuc icin
	 # s1 t1'deki adreste bulunan karakter
	###########################################


.text

	main:

		li $v0, 4
		la $a0, isaret
		syscall   # isaret yazi dizisini ekrana yaz
		li $v0, 0   # v0 temizle

		li $v0, 4
		la $a0, y1
		syscall   # y1 yazi dizisini ekrana yaz
		li $v0, 0   # v0 temizle

		li $v0, 8
		la $a0, veri
		li $a1, 1024
		syscall   # Kullanici girdisini (Yazi Dizisi) veri'ye kaydet
		li $v0, 0   # v0 temizle

		li $t3, 10   # t3 10 ile carparken kullanilacak

		lb $s1, veri($t0)   # veri'deki baslangic adresinden t0 daki deger kadar uzakta olan adreste bulunan karakteri s1'e ata

	################################################################################################

	kontrol:

		slti $t1, $s1, 48   # (Ascii - 0) 48 den kucuk ise sayi degildir
		bgtz $t1, operatorler   # Sadece operator olabilir, aksi durumda (0) devam et
		li $t1, 0   # t1'i sifirla

		slti $t1, $s1, 58  # (Ascii - 9) 57'den de kucuk ise sayidir
		bgtz $t1, sayilar   # Ustteki durumu kontrol et (1 ise sayidir, aksi durumda sonraki byte'a atlanir)
		# Eger 57'den buyuk ise bu byte sadece harftir
		bnez $t2, sayiata

		kdevam:
			li $t2, 0   # Sayi basamagi sayacini sifirla
			bnez $t2, sayiata
			j sonraki

	################################################################################################

	sayilar:   # Sayi islemlerinin yapildiği yer
		beqz $t2, ilkbas   # t2 sifira esitse ilkbasamak islemini yap
		j cokbasamak

		ilkbas:
		  	subi $s1, $s1, 48   # Basamak Islemlerini yapmak icin sayiya cevir (48 Ascii - 0) - s1 simdiki byte
			move $t5, $s1   # sayiyi t5'e tasi
			addi $t2, $t2, 1   # Basamak arttir
			j sonraki   # Sonraki byte'a gec

		cokbasamak:
			subi $s1, $s1, 48   # Basamak Islemlerini yapmak icin sayiya cevir (48 Ascii - 0) - s1 simdiki byte

			mult $t5, $t3   # t5 deki sayiyi 10(t3) ile carp
			mflo $t5   # t5 <= t5 x 10
			add $t5, $t5, $s1   # s1 deki sayi bir basamak sayisidir, bu durumda t5'de duran sayiya ekle
			j sonraki   # Sonraki byte'a gec

		sayiata:
			bnez $t6, sayi2   # t6 0'a esit degilse sayi2'e atla

			# Aksi durumda 1. sayi varsay
			move $t6, $t5   # t5'de bulunan sayiyi t6(1.Sayi icin)'ya ata
			li $t5, 0   # Gecici sayiyi sifirla
			beq $s1, 0, idevam   # Eger s1 NULL'a esit ise islemler bolumunden geldigimiz icin geri don
			j kdevam

			sayi2:
				move $t7, $t5   # t5'de bulunan sayiyi t7(2.Sayi icin)'ya ata
				beq $s1, 0, idevam   # Eger s1 NULL'a esit ise islemler bolumunden geldigimiz icin geri don
				j kdevam

	################################################################################################

	operatorler:   # Operator islemlerinin yapildiği yer
		opkont:   # Operatorleri atamak icin
		beq $s1, 43, optut   # Daha sonra toplama sembolunu kontrol etmek icin optut'a atla
		beq $s1, 45, optut   # Daha sonra cikarma sembolunu kontrol etmek icin optut'a atla
		beq $s1, 42, optut   # Daha sonra carpma sembolunu kontrol etmek icin optut'a atla
		beq $s1, 47, optut   # Daha sonra bolme sembolunu kontrol etmek icin optut'a atla
		j sonraki   # Sonraki byte'a gec

		optut:   # Operatorleri tutmak icin
		move $t4, $s1   # Operatorleri t4'e tasi
		bnez $t2, sayiata   # Eger t2 0'dan farkli ise bu demek oluyor ki az once sayi ile islem yapmisiz, atanip atanmadigini kontrol et
		j sonraki   # Sonraki byte'a gec

	################################################################################################

	islemler:   # Operator islemlerini yapmak icin

		beqz $t6, sayiata   # Sayilarin atanmis olup olmadiklarini kontrol et
		beqz $t7, sayiata   # Atanmamis herhangi bir sayi varsa sayi ata

		idevam:
			beq $t4, 43, topla
			beq $t4, 45, cikar
			beq $t4, 42, carp
			beq $t4, 47, bol
			j son


			topla:
				add $s0, $t6, $t7
				j son

			cikar:
				sub $s0, $t6, $t7
				j son

			carp:
				mult $t6, $t7
				mflo $s0
				j son

			bol:
				div $t6, $t7
				mflo $s0
				j son


	################################################################################################

	sonraki:
		addi $t0, $t0, 1   # adreste gezebilmek icin 1 ekle
		lb $s1, veri($t0)   # veri'deki baslangic adresinden t0 daki deger kadar uzakta olan adreste bulunan karakteri s1'e ata
		beq $s1, 0, islemler   # Eger yazi dizisinin sonundaysak bitir (Ascii - NULL)
		j kontrol

	################################################################################################

	son:

		li $v0, 4
		la $a0, y2
		syscall   # y1 yazi dizisini ekrana yaz
		li $v0, 0   # v0 temizle

		move $a0, $s0
		li $v0, 1
		syscall
		li $v0, 0   # v0 temizle

		li $v0, 10
		syscall
